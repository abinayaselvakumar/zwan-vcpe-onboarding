#!/bin/bash
i=1;
mkdir /etc/amz
touch /etc/amz/PRODUCT_ID
touch /etc/amz/MGMT_URL
touch /etc/amz/MGMT_ACCESS_KEY
for n in "$@"
do
    echo "Keys: $i: $n";
    i=$((i + 1));
    echo "$1" > /etc/amz/PRODUCT_ID
    echo "$2" > /etc/amz/MGMT_URL
    echo "$3" > /etc/amz/MGMT_ACCESS_KEY
done

sleep 5
ip route add default via 172.17.0.1
ret="$?"
i=0
while [[ "$ret" != 0 ]]
do
        sleep 2
        echo "retrying set route..."
        ip route add default via 172.17.0.1
        ret="$?"
done
