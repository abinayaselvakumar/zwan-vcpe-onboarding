#!/bin/sh
echo -n 65536 > /proc/sys/fs/inotify/max_user_instances
cat /proc/sys/fs/inotify/max_user_instances
echo fs.inotify.max_user_instances=65536 >> /etc/sysctl.conf

. ./myEnv.sh
while [ $start_value -le $end_value ]
do
    docker run -d --name $vcpe_name$start_value --privileged --tmpfs /tmp --tmpfs /run --tmpfs /run/lock -v /sys/fs/cgroup:/sys/fs/cgroup:ro abinayaselvakumar/onboard
    cmd="sh /usr/local/sbin/onboard.sh ${vcpe_name}${start_value} ${mgmt_url} ${mgmt_access_key}"
    cmd1="touch /etc/fwinfo"
    sleep 6
    docker exec -it ${vcpe_name}${start_value} ${cmd}
    sleep 4
    docker exec -it ${vcpe_name}${start_value} ${cmd1}
    sleep 6
    # increment the value
    start_value=`expr $start_value + 1`
done
echo "Done!"
